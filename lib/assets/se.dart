import 'package:flutter/material.dart';
import '_generated_str_extension.dart';
import '../app_localizations.dart';

extension Languages on String {
  ///Translation function
  l(BuildContext context, [List args = const []]) {
    try {
      return AppLocalizations.of(context).t(this, args);
    } catch (e) {
      return this;
    }
  }
  call(BuildContext context, [List args = const []]) => l(context, args);
}

// ignore: non_constant_identifier_names
final _Strings strings = _Strings();
