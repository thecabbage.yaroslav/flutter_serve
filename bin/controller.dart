import 'dart:io';

enum LaunchFlags { strings, files, db, uploadDb2Web, testDbLocally, pub }

extension LFC on LaunchFlags {
  String stringify() {
    return toString().replaceAll('.', '_');
  }

  String onlyName() {
    return toString().replaceFirst('$LaunchFlags.', '');
  }

}

Future<void> create() async {
  print('create started');
  // записываем sh по нужному пути

  // file://.+/lib/
  // ".+flutter_serve.+\/lib"
  // .idea


  var resultString = await File('.idea/libraries/Dart_SDK.xml').readAsString();
  var regExp = RegExp(
    r'file://.+/lib/',
    caseSensitive: false,
    multiLine: false,
  );

  var dartExePath = regExp.stringMatch(resultString).toString();
  var dartExePathList = dartExePath.split('/');
  dartExePathList.removeAt(dartExePathList.length-1);
  dartExePathList.removeAt(dartExePathList.length-1);
  dartExePath = dartExePathList.join('/') + '/bin/dart.exe';
  var shDartPath = File(dartExePath).path;

  regExp = RegExp(
    r'".+flutter_serve.+\/lib"',
    caseSensitive: false,
    multiLine: false,
  );

  // print(Directory.fromUri(Uri.parse('file://\$USER_HOME\$/AppData/Local/Pub/Cache/git/flutter_serve-202a6fe9e74f9daa3d5378f9489a6d849e8b2aff/lib')).existsSync());

  print(Platform.environment);
  resultString = await File('.idea/libraries/Dart_Packages.xml').readAsString();
  var flutterServePath = regExp.stringMatch(resultString).toString();
  var flutterServePathList = flutterServePath.split('/');
  flutterServePathList.removeAt(flutterServePathList.length-1);
  flutterServePath = flutterServePathList.join('/') + '/bin/flutter_serve.dart';
  flutterServePath = flutterServePath.replaceAll('"', '');
  var flutterServeDartPath = File(flutterServePath).path;

  var s = '''
  #!/bin/bash
printf "flutter serve started\\n"

"${Platform.executable}" pub run flutter_serve \$1

#cmd /k

  ''';


  // File(shDartPath).writeAsStringSync(DartFormatter().format(s));
  File('.idea/shell_launch_script.sh').createSync(recursive: true);
  File('.idea/shell_launch_script.sh').writeAsStringSync(s);
}

void save(String fileName, String flags){
  var s =
  '''
\<component name="ProjectRunConfigurationManager">
  <configuration default="false" name="${fileName}" type="ShConfigurationType" folderName="flutter_serve">
    <option name="INDEPENDENT_SCRIPT_PATH" value="false" />
    <option name="SCRIPT_PATH" value="\$PROJECT_DIR\$/.idea/shell_launch_script.sh" />
    <option name="SCRIPT_OPTIONS" value="&quot;--${flags}&quot;" />
    <option name="INDEPENDENT_SCRIPT_WORKING_DIRECTORY" value="true" />
    <option name="SCRIPT_WORKING_DIRECTORY" value="\$PROJECT_DIR\$" />
    <option name="INDEPENDENT_INTERPRETER_PATH" value="true" />
    <option name="INTERPRETER_PATH" value="" />
    <option name="INTERPRETER_OPTIONS" value="" />
    <method v="2" />
  </configuration>
</component>
      ''';

  File('.idea/runConfigurations/${fileName}.xml').createSync(recursive: true);
  File('.idea/runConfigurations/${fileName}.xml').writeAsStringSync(s);
}