import 'dart:io';

void uploadDb2Web(){
  var readme = File('README.md').readAsStringSync();
  var firstGCloudRegExp = RegExp(
    r'gcloud builds submit --tag gcr.io/.+/db_serve',
    caseSensitive: false,
    multiLine: false,
  );

  var firstGCloudCommand = firstGCloudRegExp.stringMatch(readme).toString();

  var secondGCloudRegExp = RegExp(
    r'gcloud beta run deploy --image gcr.io/.+/db_serve',
    caseSensitive: false,
    multiLine: false,
  );

  var secondGCloudCommand = firstGCloudRegExp.stringMatch(readme).toString();

}