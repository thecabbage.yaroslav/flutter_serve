// Создание файла index.js
import '../database_processor.dart';
import 'functions_and_consts.dart';

Future<void> indexJsFile({List<List<String>> urlPaths, Directory flutterServeDir, List<String> funcNames}) async {
  var resultString = '';

  var indexJS = await File('.flutter_serve/cloud_functions_part/functions/index.js').readAsString();
  if (!indexJS.contains('const urls = ') ) {
    resultString += """const functions = require('firebase-functions');
const axios = require('axios');
const mapUrls = functions.config().dart_db;
""";
    resultString += "const urls = 'http://localhost:';\n";
  } else {
    var indexJsStrings = indexJS.split(';');
    resultString += indexJsStrings[0] + ';' + indexJsStrings[1] + ';' + indexJsStrings[2] + ';' + indexJsStrings[3] + ';\n';
  }

  resultString += '''
  
function sendRequest(data) {
    const mapUrls = functions.config().dart_db;
    const futures = [];
    if (mapUrls) {
        for (let _url in mapUrls) {
            futures.push(axios.post(urls[_url], data));
        }
    }
    if (urls) {
        if (typeof urls === "string") {
            futures.push(axios.post(urls, data));
        } else {
            for (let _url in urls) {
                futures.push(axios.post(urls[_url], data));
            }
        }
    }

    return Promise.all(futures);
}
  ''';

  // resultString += "const url = 'https://dbserve-yktyid54ia-uc.a.run.app';\n";
  for (var i = 0; i < urlPaths.length; i++) {
    resultString += exportsFuncCreator(urlPaths[i], funcName: funcNames[i]);
  }
  createFileFromString(flutterServeDir, 'index', resultString, fileType: '.js', isNeedFormat: false);
}

String exportsFuncCreator(List<String> path, {String funcName = 'func'}) {
  var stringPath = path.join('/');
  var resultString = '';
  resultString += """\nexports.${funcName} = functions.database.ref('${stringPath.toLowerCase()}')
    .onWrite((snap, context) => {
        const snapJson = JSON.stringify(snap);
        const contextJson = JSON.stringify(context);
        const data = {
            snap: snap,
            context: context,
            url: '${stringPath.toLowerCase()}'
        };

        return sendRequest(data);
    });""";

  return resultString;
}

bool textToIndexSolverFunction(
    {String currentBranchID, String funcdef, Directory flutterServeDir, List<List<String>> urlPaths, List<String> funcNames}) {
  var path = reversePath(currentBranchID);
  var countKeys = 0;

  var urlPath = <String>[];
  var result = true;
  if (funcdef.contains('#onChange:')) {
    funcdef = stringCleaner(funcdef.replaceAll('#onChange:', ''));
    var pathsString = funcdef.substring(0, funcdef.indexOf('#'));
    var funcName = funcdef.substring(funcdef.indexOf('#'));
    funcName = funcName.split('#')[1];
    // header functions adding
    if (!funcdef.contains('@custom@')) {
      generatedHandlerSwitchUrls.add(urlPathConvert(pathsString));
      generatedHandlerSwitchFuncs.add(funcName);
      // headerSwitchUrls.add('/' + pathsString.replaceAll('.key.', '.{key}.').replaceAll('.', '/').replaceAll(' ', ''));
      // headerSwitchFuncs.add(funcName);
      result = false;
    } else {
      customHandlerSwitchUrls.add(urlPathConvert(pathsString));
      customHandlerSwitchFuncs.add(funcName);
    }
    funcNames.add(funcName);
    // var onChangePaths = stringCleaner(funcdef).split('<li>');
    var onChangePaths = stringCleaner(pathsString).split('.');
    // var urlPaths = <List<String>>[];
    path.add('{key${countKeys}');
    countKeys++;
    for (var element in onChangePaths) {
      // var endOfPath = element.split('-&gt;').last;
      // endOfPath = endOfPath.replaceAll(' ', '');

      if (element.contains('key')) {
        element = '{key${countKeys}}';
        countKeys++;
      }
      urlPath.add(element.replaceAll(' ', ''));
    }
    urlPaths.add(urlPath);
  }

  return result;
  // exportsFuncCreator(path);
}

String urlPathConvert(String path) {
  var keysCounter = 1;
  while (path.contains('.key')) {
    path = path.replaceFirst('.key', '.{key${keysCounter}}');
    keysCounter++;
  }
  return path.replaceAll('.', '/').replaceAll(' ', '');
}
