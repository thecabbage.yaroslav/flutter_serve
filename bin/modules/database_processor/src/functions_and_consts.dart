export 'model_files_functions.dart';
export 'string_and_files_functions.dart';
export 'const_files.dart';
export 'dart:io';
export 'file_creator.dart';
export '../../string_extension.dart';
export 'miro_widgets_classes.dart';
export 'js_file_creator.dart';
export 'db_copy_functions.dart';
export 'enum_file_maker.dart';
export 'copy_from_flutter_serve.dart';
export 'files_creating.dart';
import 'dart:io';

final flutterServeVersion = '1.4.6';

final generatedDatabaseDir = Directory('lib/src');
final flutterServeDir =
Directory('.flutter_serve/cloud_functions_part/functions');

final databaseManagerDir = Directory('lib/src/manager/src');

// Местонахождение и название файла с функциями для БД
final dbFunctionsDir = Directory('lib/src/models');
final functionsFileName = 'db_functions';

// Местонахождение и название файла Dockerfile
final dockerfileDir = Directory.current;
final dockerfileName = 'Dockerfile';

// // Местонахождение и название файла pubspec.yaml
final pubspecDir = Directory('');
final pubspecName = 'pubspec';

// Местонахождение и название файла server.dart в bin
final serverFileDir = Directory('bin');
final serverFileName = 'server';

// Местонахождение и название файла db_handler.dart в bin
final dbHandlerDir = Directory('bin');
final dbHandlerName = 'db_handler';

// Местонахождение и название файла server_auth.dart в bin
final serverAuthDir = Directory('bin/utils');
final serverAuthName = 'server_auth';

// Местонахождение и название файла db_handler.dart в bin
final dbTestDir = Directory('test');
final dbTestName = '';

// Местонахождение и название файла custom_handle.dart в bin/db_request_handler
final customHandleDir = Directory('bin/db_request_handler');
final customHandleName = 'custom_handle';

// Местонахождение и название файла generated_handle.dart в bin/db_request_handler
final generatedHandleDir = Directory('bin/db_request_handler');
final generatedHandleName = 'generated_handle';

// Местонахождение и название файла types.dart в lib
final typesDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final typesName = 'types';

// Местонахождение и название файла base_native_type.dart в lib
final baseNativeTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final baseNativeTypeName = 'base_native_type';

// Местонахождение и название файла duration_type.dart в lib
final durationTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final durationTypeName = 'duration_type';

// Местонахождение и название файла bool_type.dart в lib
final boolTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final boolTypeName = 'bool_type';

// Местонахождение и название файла obj_type.dart в lib
final objTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final objTypeName = 'obj_type';

// Местонахождение и название файла map_type.dart в lib
final mapTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final mapTypeName = 'map_type';

// Местонахождение и название файла list_type.dart в lib
final listTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final listTypeName = 'list_type';

// Местонахождение и название файла list_type.dart в lib
final etagTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final etagTypeName = 'etag_type';

// Местонахождение и название файла number_type.dart в lib
final numberTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final numberTypeName = 'number_type';

// Местонахождение и название файла number_type.dart в lib
final sessionKeyTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final sessionKeyTypeName = 'session_key_type';

// Местонахождение и название файла string_type.dart в lib
final stringTypeDir = Directory(generatedDatabaseDir.path + '/models/native_types');
final stringTypeName = 'string_type';

// Местонахождение и название файла database_events.dart в lib
final databaseEventsDir = databaseManagerDir;
final databaseEventsName = 'database_events';

// Местонахождение и название файла database_events.dart в lib
final databaseSaverDir = databaseManagerDir;
final databaseSaverName = 'database_saver';

// Местонахождение и название файла database_utils.dart в lib
final databaseUtilsDir = databaseManagerDir;
final databaseUtilsName = 'database_utils';

// Местонахождение и название файла firestore_database_manager.dart в lib
final firestoreDatabaseManagerDir = databaseManagerDir;
final firestoreDatabaseManagerName = 'firestore_database_manager';

// Местонахождение и название файла local_database_manager.dart в lib
final localDatabaseManagerDir = databaseManagerDir;
final localDatabaseManagerName = 'local_database_manager';

// Местонахождение и название файла realtime_database_manager.dart в lib
final realtimeDatabaseManagerDir = databaseManagerDir;
final realtimeDatabaseManagerName = 'realtime_database_manager';

// Местонахождение и название файла session_manager.dart в lib
final sessionManagerDir = databaseManagerDir;
final sessionManagerName = 'session_manager';

// Местонахождение и название файла settings_manager.dart в lib
final settingsManagerDir = databaseManagerDir;
final settingsManagerName = 'settings_manager';

// Местонахождение и название файла stream_utils.dart в lib
final streamUtilsDir = databaseManagerDir;
final streamUtilsName = 'stream_utils';

// Местонахождение и название файла custom_client.dart в utils
final customClientDir = Directory('bin/utils');
final customClientName = 'custom_client';

// Местонахождение и название файла fake_browser_client.dart
final fakeBrowserClientDir = Directory('lib/src/manager/src');
final fakeBrowserClientName = 'fake_browser_client';

// Местонахождение и название файла manager.dart
final managerDir = Directory('lib/src/manager');
final managerName = 'manager';

// Местонахождение и название файла ${libName}.dart в lib
final libNameDir = Directory('lib');

// Местонахождение и название файлов models для ${libName}.dart в lib
final libNameModelsDir = Directory('src/models');

// Местонахождение и название файла models в lib
final modelsDirPath = Directory('lib/src/models');

// Местонахождение и название файла g-service-account.json в assets
final gServiceAccountDir = Directory('assets');
final gServiceAccountName = 'g-service-account';

// Местонахождение файла enum.dart в models
var enumFileDir = Directory(generatedDatabaseDir.path + '/models');