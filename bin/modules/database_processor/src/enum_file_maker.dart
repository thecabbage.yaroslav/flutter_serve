import 'functions_and_consts.dart';

void enumFileMaker(Set<String> enumSet, Directory dirPath) {
  var resultString = '';
  resultString += 'enum Keywords { ';
  for (var keyword in enumSet) {
    resultString += keyword;
    resultString += ', ';
  }
  resultString = resultString.substring(0, resultString.length - 2);
  resultString += '} \n';
  resultString +=
  "extension single on Keywords { String get short { return this.toString().replaceFirst('\${Keywords}.', '');}}";
  createFileFromString(dirPath, 'enum_model', resultString);
}