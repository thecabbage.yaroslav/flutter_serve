import 'functions_and_consts.dart';
import 'package:resource_portable/resource.dart' show Resource;

// создание pubspec.yaml в cloud_run_part. Сейчас не используется
Future<void> pubspecUpdater({String fileName, Directory fileDir, String libName}) async {

  // берём строки из файла с путём: flutter_serve/lib/database/path/path.dart
  var pubspecYaml = (await Resource('pubspec.yaml')
      .readAsString());
  var resultString = pubspecYaml.replaceFirst(RegExp(r'version: \d+(.\d+)*'), 'version: ${flutterServeVersion}');
  createFileFromString(fileDir, fileName, resultString,
      fileType: '.yaml', isNeedFormat: false);
}

// path.dart
void copyPathFile(
    {Directory generatedDatabaseDir, String libName, String projectID}) async {
  // создаётся файл path.dart внутри директории generated_database
  File(generatedDatabaseDir.path + '/path.dart').createSync();

  // берём строки из файла с путём: flutter_serve/lib/database/path/path.dart
  var pathDart = (await Resource('package:flutter_serve/database/path.dart')
      .readAsString());
  pathDart = "import '../src/../${libName}.dart';" + pathDart;
  pathDart =
      "\nimport 'package:${libName}/src/manager/manager.dart';" + pathDart;
  pathDart = pathDart.replaceFirst(
      "var projectId = '';", "var projectId = '${projectID}';");
  pathDart = pathDart.replaceFirst(
      "var baseRoot = '';", "var baseRoot = '${projectID}-default-rtdb';");
  // внутрь созданного файла записываем строки из переменной pathDart
  File(generatedDatabaseDir.path + '/path.dart').writeAsStringSync(pathDart);
}

// types.dart
void copyTypesFile({Directory generatedDatabaseDir, String libName}) async {
  final typesDart = await Resource(
          'package:flutter_serve/database/models/native_types/types.dart')
      .readAsString();

  File(generatedDatabaseDir.path + '/models/native_types/types.dart')
    ..createSync(recursive: true)
    ..writeAsStringSync("import '../../../${libName}.dart';" + typesDart);
}

// database_manager.dart
void copyDatabaseManagerFile(
    {Directory generatedDatabaseDir, String libName}) async {
  final databaseManagerDart = await Resource(
          'package:flutter_serve/database/manager/database_manager.dart')
      .readAsString();

  if (!File(generatedDatabaseDir.path + '/database_manager.dart')
      .existsSync()) {
    File(generatedDatabaseDir.path + '/database_manager.dart')
        .createSync(recursive: true);
  }
  File(generatedDatabaseDir.path + '/database_manager.dart').writeAsStringSync(
      "import '../../../${libName}.dart';" +
          "\nimport 'package:${libName}/src/manager/manager.dart';" +
          "\nimport 'package:${libName}/src/models/native_types/types.dart';" +
          "\nimport 'package:${libName}/src/manager/src/settings_manager.dart';" +
          databaseManagerDart);
}

// g_storage_helper.dart
void copyGStorageHelperFile(
    {Directory generatedDatabaseDir, String libName}) async {
  final gStorageHelperDart = await Resource(
          'package:flutter_serve/database/manager/g_storage_helper.dart')
      .readAsString();

  File(generatedDatabaseDir.path + '/g_storage_helper.dart').writeAsStringSync(
          "import 'package:${libName}/${libName}.dart';\n" +
          gStorageHelperDart);
}

// firestore_database_manager.dart
void copyFirestoreDatabaseManagerFile(
    {String fileName, Directory fileDir}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/manager/firestore_database_manager.dart')
      .readAsString();
  createFileFromString(fileDir, fileName, resultString);
}

// database_events.dart
void copyDatabaseEventsFile({String fileName, Directory fileDir}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/manager/database_events.dart')
      .readAsString();
  createFileFromString(fileDir, fileName, resultString);
}

// database_saver.dart
void copyDatabaseSaverFile({String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
      'package:flutter_serve/database/manager/database_saver.dart')
      .readAsString();
  resultString = "import '../../../${libName}.dart';" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// fake_browser_client.dart
Future<void> fakeBrowserClientFileMaker({String fileName, Directory fileDir}) async {
  var resultString = await Resource(
      'package:flutter_serve/database/manager/fake_browser_client.dart')
      .readAsString();
  createFileFromString(fileDir, fileName, resultString);
}

// database_utils.dart
void copyDatabaseUtilsFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/manager/database_utils.dart')
      .readAsString();
  resultString = "import '../../././../${libName}.dart';" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// local_database_manager.dart
void copyLocalDatabaseManagerFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/manager/local_database_manager.dart')
      .readAsString();
  resultString = "import '../../../${libName}.dart';" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// realtime_database_manager.dart
void copyRealtimeDatabaseManagerFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/manager/realtime_database_manager.dart')
      .readAsString();
  resultString = "import '../../../${libName}.dart';\n" + resultString;
  resultString =
      "import 'package:${libName}/src/manager/manager.dart';\n" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// session_manager.dart
void copySessionManagerFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/manager/session_manager.dart')
      .readAsString();
  resultString =
      "import 'package:${libName}/src/models/native_types/session_key_type.dart';" +
          resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// settings_manager.dart
void copySettingsManagerFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/manager/settings_manager.dart')
      .readAsString();
  resultString =
      "\nimport '../../../${libName}.dart';" +
          resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// stream_utils.dart
void copyStreamUtilsFile({String fileName, Directory fileDir, String libName}) async {
  var resultString =
      await Resource('package:flutter_serve/database/manager/stream_utils.dart')
          .readAsString();
  resultString = "import 'package:${libName}/${libName}.dart';\n" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// custom_client.dart
void copyCustomClientFile({String fileName, Directory fileDir}) async {
  var resultString =
      await Resource('package:flutter_serve/database/manager/custom_client.dart')
          .readAsString();
  createFileFromString(fileDir, fileName, resultString);
}

// base_native_type.dart
void copyBaseNativeTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/base_native_type.dart')
      .readAsString();
  resultString = "\nimport '../../../${libName}.dart';" + resultString;
  resultString =
      "\nimport 'package:${libName}/src/manager/src/database_events.dart';" +
          resultString;
  resultString =
      "\nimport 'package:${libName}/src/manager/src/settings_manager.dart';" +
          resultString;
  resultString =
      "\nimport 'package:${libName}/src/manager/manager.dart';" + resultString;
  resultString =
      "\nimport 'package:${libName}/src/models/native_types/etag_type.dart';" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// base_native_type.dart
void copyDurationTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/duration_type.dart')
      .readAsString();
  resultString =
      "import 'package:${libName}/${libName}.dart';\n" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// base_native_type.dart
void copyBoolTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/bool_type.dart')
      .readAsString();
  resultString = "import 'package:${libName}/${libName}.dart';\n" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// obj_type.dart
void copyObjTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/obj_type.dart')
      .readAsString();
  resultString = "import 'package:${libName}/${libName}.dart';\n" + resultString;
  resultString = "import 'package:${libName}/src/manager/src/g_storage_helper.dart';\n" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// map_type.dart
void copyMapTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/map_type.dart')
      .readAsString();
  resultString = "\nimport '../../../${libName}.dart';" + resultString;
  resultString =
      "\nimport 'package:${libName}/src/manager/src/database_events.dart';" +
          resultString;
  resultString =
      "\nimport 'package:${libName}/src/manager/src/settings_manager.dart';" +
          resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// list_type.dart
void copyListTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/list_type.dart')
      .readAsString();
  resultString = "import 'package:${libName}/${libName}.dart';" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// etag_type.dart
void copyEtagTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/etag_type.dart')
      .readAsString();
  resultString = "import 'package:${libName}/${libName}.dart';" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// number_type.dart
void copyNumberTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/number_type.dart')
      .readAsString();
  resultString = "import '../../../${libName}.dart';" + resultString;
  resultString =
      "\nimport 'package:${libName}/src/manager/src/database_events.dart';" +
          resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// session_key_type.dart
void copySessionKeyTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/session_key_type.dart')
      .readAsString();
  resultString = "import '../../../${libName}.dart';" + resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// string_type.dart
void copyStringTypeFile(
    {String fileName, Directory fileDir, String libName}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/string_type.dart')
      .readAsString();
  resultString = "import '../../../${libName}.dart';" + resultString;
  resultString =
      "\nimport 'package:${libName}/src/manager/src/database_events.dart';" +
          resultString;
  createFileFromString(fileDir, fileName, resultString);
}

// types.dart
void typesFileMaker({String fileName, Directory fileDir}) async {
  var resultString = await Resource(
          'package:flutter_serve/database/models/native_types/types.dart')
      .readAsString();
  createFileFromString(fileDir, fileName, resultString);
}
