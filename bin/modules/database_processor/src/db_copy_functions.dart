import 'functions_and_consts.dart';

String copyFunc(List<String> pastePath, String funcName) {
  var resultString = '';

  // добавление объявления функции
  resultString += 'static void ${funcName}(Map jsonLog) async{\n';

  // добавление выбора основных данных из json из логов
  resultString += """var authType = jsonLog['context']['authType'];
    if (authType != 'USER'){
    return;
    }

    var keyID0 = jsonLog['context']['auth']['uid'];
    var keyID1 = jsonLog['context']['params']['key'];
    var value = jsonLog['snap']['after'];\n""";

  // adding db request
  resultString += 'await db.root';
  for (var i = 0; i < pastePath.length; i += 2) {
    resultString += '.';
    resultString += pastePath[i].toLowerCase().toCamelCase();
    resultString += '[keyID${i ~/ 2}]';
    // if (i + 2 < pastePath.length) {
    //   resultString += '[keyID${i ~/ 2}]';
    // } else {
    //   resultString += '.asyncSetter(keyID${i ~/ 2}, value);\n';
    // }
  }
  // resultString += '.asyncSetter(keyID${i ~/ 2}, value);\n';
  resultString += '.set(value);\n';
  // resultString += ' = ';
  // resultString += 'value;\n';
  resultString += '}';

  return resultString;
}

void createCopyFuncsFile(
    {List<String> funcsList,
      Directory dirPath,
      String fileName,
      String libName}) {
  var resultString = '';
  resultString += "import '../../${libName}.dart';\n";
  resultString += '/*';
  resultString += 'abstract class DbFunctions{';

  for (var funcString in funcsList) {
    resultString += funcString;
    resultString += '\n';
  }
  resultString += '}';
  resultString += '*/';
  createFileFromString(dirPath, fileName, resultString);
}