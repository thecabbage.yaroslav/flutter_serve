import '../database_processor.dart';
import 'functions_and_consts.dart';

String stringCleaner(String s) {
  s = s.replaceAll('<p>', '');
  s = s.replaceAll('</p>', '');
  s = s.replaceAll('<u>', '');
  s = s.replaceAll('</u>', '');
  s = s.replaceAll('<ul>', '');
  s = s.replaceAll('</ul>', '');
  s = s.replaceAll('</li>', '');
  s = s.replaceAll(' ', '');
  s = s.replaceAll('&#61;', '=');
  s = s.replaceAll('&#64;', '@');
  return s;
}

String makeImportModel(String s) {
  var resultImport = "import '";
  resultImport += s.toLowerCase();
  resultImport += "_model.dart';";
  return resultImport;
}

String customHandleSwitchCaseMaker({String url, String funcName}) {
  var resultString = '';
  resultString += """case '${url.toLowerCase()}':
      return ${funcName}(map);
      break;""";
  return resultString;
}

String headerFuncMaker({String funcName}) {
  var resultString = '';
  resultString += '''Response ${funcName}(Map jsonLog) {
  return Response(200);
  }''';
  return resultString;
}

String standardTypeDefiner({String varType, bool boolVar}) {
  boolVar = false;
  if (varType == 'String') {
    varType = 'DbString';
    boolVar = true;
  }
  if (varType == 'double') {
    varType = 'DbNumber<double>';
    boolVar = true;
  }
  if (varType == 'int') {
    varType = 'DbNumber<int>';
    boolVar = true;
  }
  if (varType == 'Duration') {
    varType = 'DbDuration';
    boolVar = true;
  }
  if (varType == 'DateTime') {
    varType = 'DbDateTime';
    boolVar = true;
  }
  if (varType == 'bool') {
    varType = 'DbBool';
    boolVar = true;
  }
  if (varType == 'Obj') {
    varType = 'DbObj';
    boolVar = true;
  }
  if (varType == '_Obj') {
    varType = 'DbObj';
    boolVar = true;
  }
  if (!boolVar){
    varType = varType.toCamelCase().capitalizeFirst();
  }
  varType = varType.replaceAll('<Int>', '<int>');
  varType = varType.replaceAll('<Double>', '<double>');
  return varType;
}

bool flagCreateModelFile(String id) {
  var counter = 0;
  var s = stringCleaner(miroTexts[id].text);
  if (s == s.capitalizeFirst()) {
    return true;
  }
  if (s.startsWith('root')) {
    return true;
  }
  return false;
  // for (var miroLineItem in miroLines[id]) {
  //   if (miroLineItem.borderStyle == 'normal') {
  //     counter++;
  //   }
  // }
  // return counter > 1;
}

bool flagCreateMapModelFile(String id) {
  if (miroTexts[id].text.contains('<u>')) {
    var childID = miroLines[id].first.endId;
    if (!stringCleaner(miroTexts[childID].text).startsWith(':')) {
      return true;
    }
  }
  return false;
}

bool flagCreateListModelFile(String id) {
  if (miroTexts[id].text.contains('<u>')) {
    var childID = miroLines[id].first.endId;
    if (stringCleaner(miroTexts[childID].text).startsWith(':')) {
      return true;
    }
  }
  return false;
}

List reversePath(String currentBranchID) {
  var path = <String>[];
  while ((miroLinesEnds[currentBranchID] != null) &&
      (miroLinesEnds[currentBranchID].isNotEmpty)) {
    path.insert(0, stringCleaner(miroTexts[currentBranchID].text));
    currentBranchID = (miroLinesEnds[currentBranchID].first.endId);
  }
  return path;
}

String findMapFileType({String currentID}) {
  var variableType = '';
  for (var lineToChild in miroLines[currentID]) {
    if (lineToChild.borderStyle != 'normal') {

      continue;
    }
    var childID = lineToChild.endId;
    var childText = miroTexts[childID].text;
    print(childText);
    if (childText.contains('<u>')) {
      variableType = stringCleaner(childText) + '_map';
      return standardTypeDefiner(
          varType: variableType.toCamelCase().capitalizeFirst());
    }
    childText = stringCleaner(childText);
    if (childText.startsWith(':')){
      childText = childText.replaceFirst(':', '');
    }
    if (childText.capitalizeFirst() == childText) {
      variableType = childText.toCamelCase().capitalizeFirst();
      break;
    } else {
      if (childText.contains('|')) {
        if (childText.split('|').length == 2) {
          if (childText.split('|').last.split(':').length == 2) {
            variableType = childText.split('|').last.split(':').last;
            break;
          } else {
            variableType = 'DbString';
            break;
          }
        } else {
          continue;
        }
      }
    }
  }
  variableType = standardTypeDefiner(varType: stringCleaner(variableType));
  variableType = variableType.toCamelCase().capitalizeFirst();
  variableType = variableType.replaceFirst('<Int>', '<int>');
  variableType = variableType.replaceFirst('<Double>', '<double>');
  return variableType;
}
