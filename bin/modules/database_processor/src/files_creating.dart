import 'functions_and_consts.dart';

void createFiles(
    {List<String> copyFunctions,
    String libName,
    List<List<String>> urlPaths,
    List<String> funcNames,
    String projectID,
    List<String> dbHandlerFunctions,
    Set<String> modelFilesNames,
    Set<String> enumSet}) {
  // g-service-account.json
  if (!File(gServiceAccountDir.path + '/${gServiceAccountName}.json').existsSync()) {
    File(gServiceAccountDir.path + '/${gServiceAccountName}.json').createSync(recursive: true);
  }

  // создаётся директория generated_database внутри head_module
  if (!generatedDatabaseDir.existsSync()) {
    generatedDatabaseDir.createSync(recursive: true);
  }

  // создаётся файл database_manager.dart внутри директории manager
  copyDatabaseManagerFile(generatedDatabaseDir: databaseManagerDir, libName: libName);

  // создаётся файл g_storage_helper.dart внутри директории manager
  copyGStorageHelperFile(generatedDatabaseDir: databaseManagerDir, libName: libName);

  // создаётся файл database_events.dart внутри директории manager
  copyDatabaseEventsFile(fileDir: databaseEventsDir, fileName: databaseEventsName);

  // создаётся файл database_saver.dart внутри директории manager
  copyDatabaseSaverFile(fileDir: databaseSaverDir, fileName: databaseSaverName, libName: libName);

  // создаётся файл database_utils.dart внутри директории manager
  copyDatabaseUtilsFile(fileDir: databaseUtilsDir, fileName: databaseUtilsName, libName: libName);

  // копирование path.dart
  copyPathFile(generatedDatabaseDir: generatedDatabaseDir, libName: libName, projectID: projectID);

  // создание файла со всеми функциями копирования db_functions
  createCopyFuncsFile(funcsList: copyFunctions, dirPath: dbFunctionsDir, fileName: functionsFileName, libName: libName);

  //создание index.js
  indexJsFile(urlPaths: urlPaths, flutterServeDir: flutterServeDir, funcNames: funcNames);

  // создание Dockerfile
  dockerfileMaker(fileName: dockerfileName, fileDir: dockerfileDir);

  // обновление pubspec.yaml
  pubspecUpdater(fileName: pubspecName, fileDir: pubspecDir, libName: libName);

  // создание server.dart в bin
  if (!File('bin/server.dart').existsSync()) {
    serverFileMaker(fileName: serverFileName, fileDir: serverFileDir, projectID: projectID, dbName: libName, projectName: libName);
  }

  // создание db_handler.dart в bin
  /*
  dbHandlerFileMaker(
      fileName: dbHandlerName,
      fileDir: dbHandlerDir,
      switchCases: dbHandlerFunctions,
      projectName: libName);

   */

  // создание server_auth.dart в bin
  serverAuthFileMaker(fileName: serverAuthName, fileDir: serverAuthDir, projectID: projectID, dbName: libName, projectName: libName);

  // создание server_auth.dart в bin
  if (!File('test/${libName}_test.dart').existsSync()) {
    dbTestFileMaker(fileDir: dbTestDir, projectName: libName);
  }

  // создание custom_handle.dart в bin/db_request_handler
  customHandleFileMaker(fileName: customHandleName, fileDir: customHandleDir);

  // создание generated_handle.dart в bin/db_request_handler
  generatedHandleFileMaker(fileName: generatedHandleName, fileDir: generatedHandleDir);

  // создание types.dart в native_types
  typesFileMaker(fileName: typesName, fileDir: typesDir);

  // создание base_native_type.dart в native_types
  copyBaseNativeTypeFile(fileName: baseNativeTypeName, fileDir: baseNativeTypeDir, libName: libName);

  // создание duration_type.dart в native_types
  copyDurationTypeFile(fileName: durationTypeName, fileDir: durationTypeDir, libName: libName);

  // создание bool_type.dart в native_types
  copyBoolTypeFile(fileName: boolTypeName, fileDir: boolTypeDir, libName: libName);

  // создание obj_type.dart в native_types
  copyObjTypeFile(fileName: objTypeName, fileDir: objTypeDir, libName: libName);

  // создание map_type.dart в native_types
  copyMapTypeFile(fileName: mapTypeName, fileDir: mapTypeDir, libName: libName);

  // создание list_type.dart в native_types
  copyListTypeFile(fileName: listTypeName, fileDir: listTypeDir, libName: libName);

  // создание etag_type.dart в native_types
  copyEtagTypeFile(fileName: etagTypeName, fileDir: etagTypeDir, libName: libName);

  // создание number_type.dart в native_types
  copyNumberTypeFile(fileName: numberTypeName, fileDir: numberTypeDir, libName: libName);

  // создание session_key_type.dart в native_types
  copySessionKeyTypeFile(fileName: sessionKeyTypeName, fileDir: sessionKeyTypeDir, libName: libName);

  // создание string_type.dart в native_types
  copyStringTypeFile(fileName: stringTypeName, fileDir: stringTypeDir, libName: libName);

  // создание firestore_database_manager.dart в manager/src
  copyFirestoreDatabaseManagerFile(fileName: firestoreDatabaseManagerName, fileDir: firestoreDatabaseManagerDir);

  // создание local_database_manager.dart в manager/src
  copyLocalDatabaseManagerFile(fileName: localDatabaseManagerName, fileDir: localDatabaseManagerDir, libName: libName);

  // создание realtime_database_manager.dart в manager/src
  copyRealtimeDatabaseManagerFile(fileName: realtimeDatabaseManagerName, fileDir: realtimeDatabaseManagerDir, libName: libName);

  // создание session_manager.dart в manager/src
  copySessionManagerFile(fileName: sessionManagerName, fileDir: sessionManagerDir, libName: libName);

  // создание settings_manager.dart в manager/src
  copySettingsManagerFile(fileName: settingsManagerName, fileDir: settingsManagerDir, libName: libName);

  // создание stream_utils.dart в manager/src
  copyStreamUtilsFile(fileName: streamUtilsName, fileDir: streamUtilsDir, libName: libName);

  // создание fake_browser_client.dart в manager/src
  fakeBrowserClientFileMaker(fileName: fakeBrowserClientName, fileDir: fakeBrowserClientDir);

  // создание manager.dart в manager
  managerFileMaker(fileName: managerName, fileDir: managerDir);

  // создание custom_client.dart в utils
  copyCustomClientFile(fileName: customClientName, fileDir: customClientDir);

  // создание ${libName}.dart в lib
  libNameFileMaker(fileName: libName, fileDir: libNameDir, models: modelFilesNames, modelsDir: libNameModelsDir);

  // создание файла enum.dart
  enumFileMaker(enumSet, enumFileDir);
}
