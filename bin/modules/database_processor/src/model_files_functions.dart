import '../database_processor.dart';
import 'functions_and_consts.dart';

void modelFileGenerator(String id, {Directory modelsDirPath, String libName}) {
  var fileStrings = <String>[];
  fileStrings.add("import 'package:${libName}/${libName}.dart';");
  fileStrings.add("import 'native_types/duration_type.dart';");
  fileStrings.add('');

  var tmpString = '';
  tmpString = 'class ';
  var className = stringCleaner(miroTexts[id].text);
  className = className.replaceAll(':', '');
  tmpString += className.toCamelCase().capitalizeFirst();
  tmpString += ' extends ';
  var extendingType = 'DbBaseType';
  var isMapClass = false;
  // if (miroTexts[id].text.contains('<u>')) {
  //   isMapClass = true;
  //   var valType = '';
  //   var typeAndVar =
  //       stringCleaner(miroTexts[miroLines[id].first.endId].text).split('|');
  //   var variableNameAndType = typeAndVar.last.split(':');
  //   if (typeAndVar.length == 2) {
  //     valType = variableNameAndType.last;
  //   } else {
  //     valType = 'String';
  //   }
  //   var isStandardType = false;
  //   valType = valType.toCamelCase();
  //   valType = standardTypeDefiner(varType: valType, boolVar: isStandardType);
  //   print("@8${valType}");
  //   extendingType = 'DbMap<DbString, ${(valType)}>';
  // }
  tmpString += extendingType;
  tmpString += '{\n';
  tmpString += '${className.toCamelCase().capitalizeFirst()}([String key]) : super(key)';
  if (className.toCamelCase().capitalizeFirst() == 'Root') {
    tmpString += '''{
    LocalDatabaseManager.rootMaps[this] = {};
  }
    ///Есть вариант, когда можно получить инстанцию на обьект в любом случае.
''';
  } else {
    tmpString += ';';
  }
  fileStrings.add(tmpString);

  fileStrings.add('');
  bool isStandartType;
  if (miroLines.containsKey(id)) {
    for (var lineToChild in miroLines[id]) {
      // Если есть классы, наследующиеся от данного
      if (lineToChild.borderStyle == 'dashed') {
        createExtendedFile(id, lineToChild.endId, modelsDirPath: modelsDirPath, libName: libName);
        continue;
      }
      if (lineToChild.borderStyle == 'dotted') {
        continue;
      }
      if ((miroTexts[lineToChild.endId] != null) && (stringCleaner(miroTexts[lineToChild.endId].text).startsWith('_'))) {
        continue;
      }
      var childText = stringCleaner(miroTexts[lineToChild.endId].text);
      var childMap = '';
      if (miroLines[lineToChild.endId] != null) {
        if (isMapClass) {
          continue;
        }
        childMap = stringCleaner(miroTexts[miroLines[lineToChild.endId].first.endId].text);
      }

      // fileStrings.add()

      if (stringCleaner(childText) == stringCleaner(childText.capitalizeFirst())) {
        print(childText);
        fileStrings.add(makeGetterVar(childText, childText.toLowerCase()));
        fileStrings.add(makeSetter(childText));
      } else {
        if (miroTexts[lineToChild.endId].text.contains('<u>')) {
          print('#${miroTexts[lineToChild.endId].text}');
          if (miroLines[lineToChild.endId] == null) {
            print('*1');
          }
          var grandsonsID = miroLines[lineToChild.endId].first.endId;
          var grandsonsText = miroTexts[grandsonsID].text;
          if (stringCleaner(grandsonsText).startsWith(':')) {
            var listType = standardTypeDefiner(varType: stringCleaner(grandsonsText).split(':').last);
            fileStrings.add(makeGetterList(varType: listType, s: childText.toLowerCase(), libName: libName, currentID: lineToChild.endId));
            // fileStrings.add(makeGetterList(varType: listType, s: childText.toLowerCase(), libName: libName));

            fileStrings.add(makeSetter(childText));
          } else {
            var keyType = 'DbString';
            if (grandsonsText.split('|').length == 2) {
              if (grandsonsText.split('|') == null) {
                print('*2');
              }
              if (grandsonsText.split('|').first.split(':').length == 2) {
                if (grandsonsText.split('|') == null) {
                  print('*3');
                }
                keyType = standardTypeDefiner(varType: stringCleaner(grandsonsText.split('|').first.split(':').last));
              }
            }
            var varType = stringCleaner(grandsonsText.split('|').last.split(':').last);
            if (grandsonsText.split('|').last.split(':') == null) {
              print('*4');
            }
            varType = findMapFileType(currentID: lineToChild.endId);
            var rootString = stringCleaner(grandsonsText.split('|').last.split(':').first.toLowerCase());
            fileStrings.add(makeGetterMap(childText, varType, childText, libName: libName, currentID: lineToChild.endId));
            fileStrings.add(makeSetter(childText));
          }
        } else {
          if (stringCleaner(childText.split(':').last)[0] == '[') {
            //TODO доработать [12, ...]
          } else {
            var childID = lineToChild.endId;
            var childText = miroTexts[childID].text;
            if (childText.split(':') == null) {
              print('*6');
            }
            var varName = stringCleaner(childText.split(':').first);
            var varType = 'DbString';
            if (childText.split(':').length == 2) {
              varType = stringCleaner(childText.split(':').last);
              varType = standardTypeDefiner(varType: varType);
            }
            fileStrings.add(makeGetterVar(varType, varName));
            fileStrings.add(makeSetter(varName));
          }
        }
      }
    }
  }

  fileStrings.add('@override\n');
  fileStrings.add(
      'Future<${className.toCamelCase().capitalizeFirst()}> get future async => await super.future as ${className.toCamelCase().capitalizeFirst()};');
  fileStrings.add('@override\n');
  fileStrings
      .add('Stream<${className.toCamelCase().capitalizeFirst()}> get stream => super.getStream<${className.toCamelCase().capitalizeFirst()}>();');
  fileStrings.add('@override\n');
  fileStrings
      .add('Stream<${className.toCamelCase().capitalizeFirst()}> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);');
  // fileStrings.add('@override\n');
  // fileStrings.add('${className.toCamelCase().capitalizeFirst()} get value => super.value;');
  // fileStrings.add('@override\n');
  // fileStrings.add('Future<${className.toCamelCase().capitalizeFirst()}> set(value) async => await super.set(value);');
  fileStrings.add("""
    @override
  ${className.toCamelCase().capitalizeFirst()} get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<${className.toCamelCase().capitalizeFirst()}> set(value) async {
    if (value is ${className.toCamelCase().capitalizeFirst()}) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
    """);

  fileStrings.add('}');
  fileStrings.add('///пакетная загрузка нужна еще');
  var resultString = '';
  for (var s in fileStrings) {
    resultString += s;
    resultString += '\n';
  }

  modelFilesNames.add((className.toLowerCase()) + '_model');

  createFileFromString(modelsDirPath, (className.toLowerCase()) + '_model', resultString);
}

String makeGetterVar(String sType, String s) {
  var resultString = '';
  sType = standardTypeDefiner(varType: sType);
  resultString += "${sType} get ${stringCleaner(s).toCamelCase()} => get(${sType}('${stringCleaner(s)}'));";
  return resultString;
}

String makeGetterMap(String sTypeOne, String sTypeTwo, String s, {String libName, String currentID}) {
  var resultString = '';

  var valueType = sTypeTwo.toCamelCase().capitalizeFirst();
  valueType = standardTypeDefiner(varType: valueType);
  var mapType = standardTypeDefiner(varType: sTypeOne.toLowerCase().toCamelCase().capitalizeFirst());
  var mapValueType = mapType + 'Map';

  var getString = gettingString(id: currentID);

  resultString += '${mapValueType} get ${stringCleaner(s).toCamelCase()} => get(${mapValueType}(${getString}));';

  return resultString;
}

String makeGetterList({String varType, String s, String libName, String currentID}) {
  var resultString = '';

  var listType = standardTypeDefiner(varType: varType.toLowerCase().toCamelCase().capitalizeFirst());
  var listValueType = stringCleaner(s).toCamelCase().capitalizeFirst() + 'List';

  var getString = gettingString(id: currentID);

  resultString += '${listValueType} get ${stringCleaner(s).toCamelCase()} => get(${listValueType}(${getString}));';

  return resultString;
}

/*
String makeGetterList({String varType, String s, String libName}) {
  var resultString = '';

  var valueType = varType.toCamelCase();
  valueType = standardTypeDefiner(varType: valueType).capitalizeFirst();
  // var mapValueType = valueType + 'Map';

  resultString +=
      "DbList<${valueType}> get ${stringCleaner(s).toCamelCase()} => get(DbList<${valueType}>('${stringCleaner(s)}', (key) => ${valueType}(key)));";

  // mapModelFileMaker(classType: mapFileNameCleaner(valueType), libName: libName);
  return resultString;
}
*/
String makeSetter(String s) {
  var resultSet = '';
  resultSet += 'set ${stringCleaner(s).toCamelCase()}(value) => ${stringCleaner(s).toLowerCase().toCamelCase()}.set(value);';
  // resultSet = 'set ';
  // resultSet += stringCleaner(s).toCamelCase();
  // resultSet += "(value) => setter('";
  // resultSet += stringCleaner(s);
  // resultSet += "', value);";
  return resultSet;
}

void mapModelFileMaker({String classType, String variableType, String libName, String id}) {
  variableType = standardTypeDefiner(varType: variableType);
  print('%${variableType}');
  var mapValueType = classType.toCamelCase().capitalizeFirst() + 'Map';

  var resultString = '';

  var valueType = classType.toCamelCase().capitalizeFirst();
  var tmpF = false;
  valueType = standardTypeDefiner(varType: valueType, boolVar: tmpF);
  variableType = variableType.toCamelCase().capitalizeFirst();
  variableType = variableType.replaceFirst('<Int>', '<int>');
  variableType = variableType.replaceFirst('<Double>', '<double>');
  resultString += """import 'package:${libName}/${libName}.dart';
  import 'native_types/types.dart';
class ${mapValueType} extends DbMap<DbString, ${variableType}> {""";
  if (id != null) {
    if (miroLines[id].length > 1) {
      for (var childWidget in miroLines[id]) {
        var childID = childWidget.endId;
        var childText = miroTexts[childID].text;
        childText = stringCleaner(childText);
        if ((!childText.contains('|')) && (!(childText.capitalizeFirst() == childText))) {
          var keyString = stringCleaner(childText.split(':').first);
          var valueType = stringCleaner(childText.split(':').last);
          valueType = standardTypeDefiner(varType: valueType);
          resultString += mapExtraGetter(valType: valueType, keyString: keyString);
          resultString += mapExtraSetter(keyString: keyString);
          resultString += '\n';
        }
      }
    }
  }

  resultString += '''
    ${mapValueType}([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<${mapValueType}> get future async =>
      await super.future as ${mapValueType};
      
  @override
  Stream<${mapValueType}> get stream => super.getStream<${mapValueType}>();
  
  @override
  Stream<${mapValueType}> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
    @override
  ${mapValueType} get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<${mapValueType}> set(value, [object]) async {
    if (value is ${mapValueType}) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
''';
  modelFilesNames.add(classType.toLowerCase() + '_map_model');
  createFileFromString(modelsDirPath, classType.toLowerCase() + '_map_model', resultString);
}

String mapFileNameCleaner(String fileName) {
  var resultString = '';
  resultString = fileName.replaceAll('<', '');
  resultString = resultString.replaceAll('>', '');
  return resultString;
}

String gettingString({String id}) {
  var childID = miroLines[id].first.endId;
  var childText = 'String';
  /*
  if (miroTexts[childID].text.split('|').length == 2) {
    if (miroTexts[childID].text.split('|').last.split(':').length == 2) {
      childText = standardTypeDefiner(varType: stringCleaner(miroTexts[childID].text).split('|').last.split(':').last);
    } else {
      childText = 'DbString';
    }
  } else {
    childText = standardTypeDefiner(varType: stringCleaner(miroTexts[childID].text)).toCamelCase();
  }
*/
  childText = findMapFileType(currentID: id);

  var keyText = stringCleaner(miroTexts[id].text);
  var resString = "'${keyText}', (key) => ";
  if (miroTexts[childID].text.contains('<u>')) {
    // childText = childText + 'Map';
    resString = resString + '${childText.capitalizeFirst()}(' + gettingString(id: childID);
  } else {
    resString += '${childText.capitalizeFirst()}(key';
  }
  return resString += ')';
}

String mapExtraGetter({String valType, String keyString}) {
  return "${valType.capitalizeFirst()} get ${keyString.toCamelCase()} => this['${keyString}']";
}

String mapExtraSetter({String keyString}) {
  return "set ${keyString.toCamelCase()}(value) => this['${keyString}'] = value;";
}

void listModelFileMaker({String className, String variableType, String libName, String id}) {
  variableType = variableType.replaceAll(':', '');
  variableType = standardTypeDefiner(varType: variableType);
  print('%${variableType}');
  var listValueType = className.toCamelCase().capitalizeFirst() + 'List';

  var resultString = '';

  var valueType = className.toCamelCase().capitalizeFirst();
  var tmpF = false;
  valueType = standardTypeDefiner(varType: valueType, boolVar: tmpF);
  variableType = variableType.toCamelCase().capitalizeFirst();
  variableType = variableType.replaceFirst('<Int>', '<int>');
  variableType = variableType.replaceFirst('<Double>', '<double>');
  resultString += """import 'package:${libName}/${libName}.dart';
  import 'native_types/types.dart';
class ${listValueType} extends DbList<${variableType}> {""";
  if (id != null) {
    if (miroLines[id].length > 1) {
      for (var childWidget in miroLines[id]) {
        var childID = childWidget.endId;
        var childText = miroTexts[childID].text;
        childText = stringCleaner(childText);
        if ((!childText.contains('|')) && (!(childText.capitalizeFirst() == childText))) {
          var keyString = stringCleaner(childText.split(':').first);
          var valueType = stringCleaner(childText.split(':').last);
          valueType = standardTypeDefiner(varType: valueType);
          resultString += mapExtraGetter(valType: valueType, keyString: keyString);
          resultString += mapExtraSetter(keyString: keyString);
          resultString += '\n';
        }
      }
    }
  }

  resultString += '''
    ${listValueType}([String key, Function(String key) objectCreator]) : super(key, objectCreator);
  @override
  Future<${listValueType}> get future async =>
      await super.future as ${listValueType};
      
  @override
  Stream<${listValueType}> get stream => super.getStream<${listValueType}>();
  
  @override
  Stream<${listValueType}> get futureBasedStream => StreamUtils.streamBasedOnFuture(this, future);
  
   @override
  ${listValueType} get value => throw 'Вы не можете делать чтение у комплексных обьектов данным путем.\\n'
      'Воспользуйтесь чтением через child variables';

  @override
  Future<${listValueType}> set(value, [object]) async {
    if (value is ${listValueType}) {
      return await super.set(value);
    } else {
      throw 'Только подходящие по типу обьекты можно записывать';
    }
  }
}
''';
  modelFilesNames.add(className.toLowerCase() + '_list_model');
  createFileFromString(modelsDirPath, className.toLowerCase() + '_list_model', resultString);
}
