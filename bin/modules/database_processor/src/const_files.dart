import '../database_processor.dart';
import 'functions_and_consts.dart';

// Dockerfile
void dockerfileMaker({String fileName, Directory fileDir}) {
  var resultString = '';
  resultString += '''# Use Google's official Dart image.
# https://hub.docker.com/r/google/dart-runtime/
FROM google/dart-runtime

# Service must listen to \$PORT environment variable.
# This default value facilitates local development.
ENV PORT 8080

# The target variable
ENV TARGET "World + Dog"

#ENV ALGOLIA_APP_ID 5COMX2KEPF

#ENV ALGOLIA_API_KEY fa5d442ce41f375cc30ff20d9ed5ee03

#ENV ALGOLIA_INDEX_NAME prod_HOUSES

''';
  createFileFromString(fileDir, fileName, resultString, fileType: '', isNeedFormat: false);
}

// server.dart
void serverFileMaker({String fileName, Directory fileDir, String projectID, String dbName, String projectName}) {
  var resultString = '';
  resultString += """
import 'dart:async';
import 'dart:io';

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart';
import 'package:googleapis/cloudtasks/v2.dart';
import 'package:googleapis/fcm/v1.dart';

import 'db_request_handler/handler_center.dart';
import 'utils/server_auth.dart';

Future main() async {
  serverAuth.scopes.add(FirebaseCloudMessagingApi.cloudPlatformScope);
  serverAuth.scopes.add(CloudTasksApi.cloudPlatformScope);
  await serverAuth.init();

  var port = int.tryParse(Platform.environment['PORT'] ?? '8080');

  var server = await serve(
    Pipeline().addMiddleware(logRequests()).addHandler(handler),
    InternetAddress.anyIPv4,
    port,
  );
}

""";
  createFileFromString(fileDir, fileName, resultString);
}

// server_auth.dart
void serverAuthFileMaker({String fileName, Directory fileDir, String projectID, String dbName, String projectName}) {
  var resultString = '';
  resultString += """
import 'dart:convert';
import 'dart:io';

import 'package:${projectName}/${projectName}.dart';
import 'package:googleapis_auth/auth_io.dart';


var gToken;

class ServerAuth {
  var scopes = [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/firebase.database',
    'https://www.googleapis.com/auth/devstorage.full_control',
    'https://www.googleapis.com/auth/devstorage.read_only',
    'https://www.googleapis.com/auth/devstorage.read_write'
  ];

  static final ServerAuth _singleton = ServerAuth._internal();
  static const String gServiceAccountPath = 'assets/g-service-account.json';
  static AutoRefreshingAuthClient client;
  ServiceAccountCredentials serviceAccountCredentials;

  Future<String> tokenViaServiceAccount() async {
    if (gToken != null) return gToken;
    client = await clientViaServiceAccount(serviceAccountCredentials, scopes);
    var token = client.credentials.accessToken.data;
    gToken = token;
    client.close();
    return token;
  }

  init() async {
    serviceAccountCredentials = ServiceAccountCredentials.fromJson(
        jsonDecode(File(gServiceAccountPath).readAsStringSync()));
    db.isWeb = false;
    db.realtimeIdTokenGeneratorServer = tokenViaServiceAccount;
    await tokenViaServiceAccount();
  }

  factory ServerAuth() {
    return _singleton;
  }

  ServerAuth._internal();
}

final serverAuth = ServerAuth();

""";
  createFileFromString(fileDir, fileName, resultString);
}

// dbTest.dart
void dbTestFileMaker({Directory fileDir, String projectName}) {
  var resultString = '';
  resultString += """
import 'dart:io';

import 'package:${projectName}/${projectName}.dart' hide Message;
import 'package:googleapis/fcm/v1.dart';

import '../bin/utils/custom_client.dart';
import '../bin/utils/server_auth.dart';

Future<void> main() async {
  print(Platform.resolvedExecutable);
  return;
  serverAuth.scopes.add(FirebaseCloudMessagingApi.cloudPlatformScope);
  await serverAuth.init();
  await test();
}

test() async {
  var sendMessageRequest = SendMessageRequest()
    // ..validateOnly = true
    ..message = Message();
  sendMessageRequest.message
    ..notification = Notification()
    ..token =
        "dQcmY1fsSmiXoDusXsR8fZ:APA91bEuVd3YlrWS54g7BE1Apl9jxunwbNDEMktaR33_pgVFT3xO5obacFe-nLLgPGRqzmVBgwdoeDRS059tfvm5yfcrG6iFYDIMdQE2kWn4y";
  sendMessageRequest.message.notification.title = 'test one';
  sendMessageRequest.message.notification.body = 'test one body';

  var client = CustomClient(headers: {
    'Authorization': 'Bearer \${await db.realtimeIdTokenGeneratorServer()}'
  });

  Platform.environment;

  try {
    var message = await FirebaseCloudMessagingApi(client)
        .projects
        .messages
        .send(sendMessageRequest, 'projects/\${db.projectId}');
    print(message);
  } on DetailedApiRequestError catch (e) {
    if (e.status == 400) {
      print("Такого токена не существует");
    }
    print(e);
    return;
  } finally {
    print("end");
  }
  client.close();
}

""";
  var fileName = projectName + '_test';
  createFileFromString(fileDir, fileName, resultString);
}

// db_handler.dart
void dbHandlerFileMaker({String fileName, Directory fileDir, List<String> switchCases, String projectName}) {
  var resultString = '';
  resultString += """import 'dart:convert';
  import 'package:${projectName}/${projectName}.dart' hide Request;
import 'handmade/header.dart' as header;
import 'package:shelf/shelf.dart';

Future<Response> handler(Request request) async {
  if (request.method != 'POST') {
    return Response(405);
  }
  var requestBody = jsonDecode(await request.readAsString() ?? '{}');
  if (requestBody is Map && requestBody.containsKey('url')) {
    var url = requestBody['url'];
    switch (url) {
    """;

  for (var stringCase in switchCases) {
    resultString += stringCase;
  }
  // case 'kkgkg':
  //   await DbFunctions.copyValue(requestBody);
  //   break;
  resultString += '''default:
        var response = header.handler(requestBody);
        if (response != null) {
          return response;
        }
    }
  }

  return Response(200);
}
''';
  createFileFromString(fileDir, fileName, resultString);
}

// db_handler.dart switches
String dbHandlerSwitchCaseMaker({String stringPath, String funcName}) {
  var resultString = '';
  var funcPath = '/';
  funcPath += stringPath.replaceAll('.key.', '.{key}.');
  funcPath = funcPath.replaceAll('.', '/');
  resultString += "case '${funcPath.toLowerCase()}':\n";
  resultString += 'await DbFunctions.${funcName}(requestBody);\n';
  resultString += 'break;\n';
  return resultString;
}

// custom_handle.dart
Future<void> customHandleFileMaker({String fileName, Directory fileDir}) async {
  var resultString = await File('bin/db_request_handler/custom_handle.dart').readAsString();
  var imports = resultString.split('Future<Response> handle(Map map) async {').first;
  var funcs = '';
  if (resultString.contains('// funcs')) {
    funcs = '// funcs\n' + resultString.split('// funcs').last;
  }

//   resultString += """import 'package:shelf/shelf.dart';
//
// Future<Response> handler(Map map) async {
//   return Response.notFound('sorry');
// }
//
// """;
  if (imports.isEmpty) {
    imports = "import 'package:shelf/shelf.dart';\n";
  }
  resultString = imports;
  resultString += """

Future<Response> handle(Map map) async {
  var url = map['url'];
  switch (url) {""";
  for (var i = 0; i < customHandlerSwitchUrls.length; i++) {
    resultString += customHandleSwitchCaseMaker(url: customHandlerSwitchUrls[i], funcName: customHandlerSwitchFuncs[i]);
  }
  resultString += '''}
  return Response(404);
}
''';
  resultString += funcs;
  for (var i = 0; i < customHandlerSwitchUrls.length; i++) {
    if (!funcs.contains('${customHandlerSwitchFuncs[i]}(Map jsonLog)')) {
      resultString += headerFuncMaker(funcName: customHandlerSwitchFuncs[i]);
    }
  }
  createFileFromString(fileDir, fileName, resultString);
}

// generated_handle.dart
void generatedHandleFileMaker({String fileName, Directory fileDir}) {
  var resultString = '';
  resultString += """import 'package:shelf/shelf.dart';

Future<Response> handle(Map map) async {
  var url = map['url'];
  switch (url) {""";
  for (var i = 0; i < generatedHandlerSwitchUrls.length; i++) {
    resultString += customHandleSwitchCaseMaker(url: generatedHandlerSwitchUrls[i], funcName: generatedHandlerSwitchFuncs[i]);
  }
  resultString += '''}
  return Response(404);
}
''';
  for (var i = 0; i < generatedHandlerSwitchUrls.length; i++) {
    resultString += headerFuncMaker(funcName: generatedHandlerSwitchFuncs[i]);
  }
  createFileFromString(fileDir, fileName, resultString);
}

// Файл с экспортами всех _model. libName exports file
void libNameFileMaker({String fileName, Directory fileDir, Set<String> models, Directory modelsDir}) {
  var resultString = '';
  resultString += """library db;

export 'src/models/db_functions.dart';
export 'src/models/native_types/types.dart';
export 'src/models/native_types/base_native_type.dart';
export 'src/path.dart';
export 'src/models/enum_model.dart';
export 'src/manager/manager.dart';
""";
  for (var model in models) {
    resultString += "export '${modelsDir.path}/${model}.dart';";
  }
  createFileFromString(fileDir, fileName, resultString);
}

// readme.md
String readmeFileGenerator({String libName, String projectID}) {
  var projName = libName.replaceFirst('db_', '');
  var resultString = """# ${libName}

A ${projName} project db package.

## Getting Started

## Building and deploying the sample

Once you have recreated the sample code files (or used the existing files) 
you're ready to build and deploy the sample app.

1. Build your container image using Cloud Build, by running the following command from 
   the top level directory containing the Dockerfile:
   
   ```gcloud builds submit --tag gcr.io/${projectID}/db_serve ```
   
   where [${projName}] is your GCP project ID. You can get it by running gcloud config get-value project.
   
   Upon success, you will see a SUCCESS message containing the image name (gcr.io/[${projName}]/db_serve). The image is stored in Container Registry and can be re-used if desired.

2. Deploy the build above using the following command:

   ```gcloud beta run deploy --image gcr.io/${projectID}/db_serve```
   
    where [${projName}] is again your GCP project ID. You can get it by running gcloud 
    config get-value project.

    When prompted, select a region (for example us-central1), confirm the service name, 
    and respond 'y' to allow unauthenticated invocations.

    Then wait a few moments until the deployment is complete. On success, the 
    command line displays the service URL. Visit your deployed container by opening the 
    service URL in a web browser.
    
    Congratulations! You have just deployed a Dart application packaged in a container 
    image to Cloud Run. Cloud Run automatically and horizontally scales your 
    container image to handle the received requests, then scales down when demand 
    decreases. You only pay for the CPU, memory, and networking consumed during 
    request handling. If you change your project you can just re-build and redeploy 
    as you wish.


## Removing the sample app deployment

To remove the built image you can [delete](https://cloud.google.com/container-registry/docs/managing#deleting_images) the image.
You can also delete your GCP project to avoid incurring charges. Deleting your GCP project stops billing for all the resources used within that project.


[![Run on Google Cloud](https://storage.googleapis.com/cloudrun/button.svg)](https://console.cloud.google.com/cloudshell/editor?shellonly=true&cloudshell_image=gcr.io/cloudrun/button&cloudshell_git_repo=https://StarCabbage:07c8bd8aa8e79a620b6cfd5c227ff5923f243609@github.com/StarCabbage/${libName}.git)""";
  return resultString;
}

// manager.dart
void managerFileMaker({String fileName, Directory fileDir, String libName}) {
  var resultString = '';
  resultString += """export 'src/stream_utils.dart';
export 'src/database_manager.dart';
export 'src/local_database_manager.dart';
export 'src/database_manager.dart';
export 'src/realtime_database_manager.dart';
export 'src/database_events.dart';
export 'src/database_utils.dart';
export 'src/session_manager.dart';
export 'src/database_saver.dart';""";
  createFileFromString(fileDir, fileName, resultString);
}
