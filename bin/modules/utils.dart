  import 'dart:io';

void runBrowser(String url) {
  var fail = false;
  var command = '';
  switch (Platform.operatingSystem) {
    case 'linux':
      command = 'xdg-open';
      break;
    case 'macos':
      command = 'open';
      break;
    case 'windows':
      command = 'start';
      break;
    default:
      fail = true;
      break;
  }

  Process.run(command, [url], runInShell: true);
  if (!fail) {
    print('Start browsing...');
    print(url);
  }
}
