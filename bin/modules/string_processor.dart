import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ansicolor/ansicolor.dart';
import 'package:dart_style/dart_style.dart';
import 'package:diff_match_patch/diff_match_patch.dart';
import 'package:http/http.dart' as http;
import 'package:yaml/yaml.dart';
import 'package:resource_portable/resource.dart' show Resource;
import 'constants.dart';
import 'obj_conv.dart';
import 'string_extension.dart';

void stringWorker() async {
  final args = loadConfigFile('pubspec.yaml', '');
  final String language = args['languageFile'] ?? '';
  final String languageFolder = args['languageFolder'] ?? '';
  if (language == null || language.isEmpty) {
    throw 'languageFile can\'t be empty';
  }

  if (languageFolder == null || languageFolder.isEmpty) {
    throw 'languageFolder can\'t be empty';
  }

  final stringsFile = File('assets/$language');
  final stringsJson = stringsFile.readAsStringSync();
  final Map realMap = jsonDecode(stringsJson);
  final Map _realMap = parseTree(jsonDecode(stringsJson), '.', true);

  makePiece(String starter, Map data, String outerKey) {
    var internalPiece = StringBuffer('$starter');
    var outerPieces = StringBuffer();
    data.forEach((key, value) {
      if (value is! Map) {
        if (value is String && (value).contains(r'${path}')) {
          internalPiece.write(
              '///${_realMap[value.replaceFirst(r"${path}", '')]}\nString get ${key.toString().toCamelCase()} => "${value.replaceFirst(r"${path}", '')}"; ${r'/*' + _realMap[value.replaceFirst(r"${path}", '')].toString()}*/\n');
        } else {
          internalPiece.write(
              '///$value\nString get ${key.toString().toCamelCase()} => "$outerKey$key"; ${r'/*' + value}*/\n');
        }
      }
      if (value is Map) {
        internalPiece.write(
            '${key.toString().toCamelCase().capitalizeFirst()} get ${key.toString().toCamelCase()} => ${key.toString().toCamelCase().capitalizeFirst()}();');
        outerPieces.write(makePiece(
            'class ${key.toString().toCamelCase().capitalizeFirst()} {',
            value,
            '$outerKey$key.'));
      }
    });
    internalPiece.write('}');
    internalPiece.write(outerPieces);
    return internalPiece;
  }

  var finalPhase = DartFormatter()
      .format(makePiece('class _Strings {', realMap, '').toString());
  final firstClass = finalPhase.substring(0, finalPhase.indexOf('}') + 1);
  finalPhase = finalPhase.replaceFirst(firstClass, '');

  final seDart =
      await Resource('package:flutter_serve/assets/se.dart').readAsString();

  var stringsExtension = StringBuffer();
  stringsExtension.write(seDart);
  stringsExtension.write(firstClass);

  final genExtensions = Directory(
    'lib/${ExportConstants.exportFolder}/gen_extensions',
  );
  final headModule = Directory(
    'lib/${ExportConstants.exportFolder}/',
  );
  genExtensions.createSync(recursive: true);
  final generatedStrExtension =
      File(genExtensions.path + '/generated_str_extension.dart');
  final _generatedStrExtension =
      File(genExtensions.path + '/_generated_str_extension.dart');
  void createFile(File file, content) {
    if (!file.existsSync()) file..createSync(recursive: true);
    file
      ..writeAsString(
          DartFormatter().format(
              '''/*Внимание! В данном файле нельзя производить изменения вручную. Т.к. генератор просто удалит их.
              Используйте плагин flutter_serve для внесения изменений: flutter pub run flutter_serve*/
              '''
              '${content.toString()}'),
          mode: FileMode.write);
  }

  createFile(generatedStrExtension, stringsExtension);
  createFile(_generatedStrExtension, finalPhase);
  final appLocalizationsDart =
      (await Resource('package:flutter_serve/assets/app_localizations.dart')
              .readAsString())
          .replaceAll('%language%', languageFolder);
  File(headModule.path + '/app_localizations.dart')
    ..writeAsStringSync(appLocalizationsDart);
  print('Обработка строк завершена');
}

AnsiPen penHighlight = AnsiPen()..magenta(bold: true);
AnsiPen penSecondHighlight = AnsiPen()..magenta();
AnsiPen littleComments = AnsiPen()..xterm(2);
var translator;

printDifference(String text1, String text2) {
  var objects = diff(text1, text2);
  var stringBuffer = StringBuffer();
  objects.forEach((element) {
    stringBuffer.write(element.text + ',\n');
  });
  print(stringBuffer);
}

variables(String matchContent) {
  var internalVariable = RegExp(r'\${.+}|\$\w+');
  var internalVariable1 = RegExp(r'\${.+}');
  var internalVariable2 = RegExp(r'\$\w+');
  if (internalVariable.allMatches(matchContent).isNotEmpty) {
    print(penHighlight(
        'Обратите внимание, что внутри данной строки имеются переменные.'
        ' Я предлагаю заменить их на спец символы. Посмотрите, верно ли я это сделал.'));
    print(matchContent);
    print(matchContent.toString().replaceAll(internalVariable, '{}'));
    print(penSecondHighlight('Как Вам кажется, верно ли произошла замена ?'
        'Кликните enter, чтобы отменить замену'
        'иначе - space и, после, enter'));
    String answer = onAnswer(true, [' ']);
    if (answer.isEmpty) {
      return [matchContent];
    }
    if (answer == ' ') {
      var stringBuffer = StringBuffer();
      internalVariable1.allMatches(matchContent).forEach((match) {
        stringBuffer.write(
            matchContent.substring(match.start + 2, match.end - 1) + ',');
      });
      internalVariable2.allMatches(matchContent).forEach((match) {
        stringBuffer
            .write(matchContent.substring(match.start + 1, match.end) + ',');
      });
      return [
        matchContent.replaceAll(internalVariable, '{}'),
        stringBuffer.toString()
      ];
    }
  }
  return [matchContent];
}

const String defaultConfigFile = 'flutter_string_extractor.yaml';

onAnswer(bool emptyString, List<String> possibleAnswers) {
  String answer;
  var endSearch = false;
  while (!endSearch) {
    answer = stdin.readLineSync(retainNewlines: false) ?? '';
    if (answer.isEmpty && emptyString) endSearch = true;
    for (var possibleAnswer in possibleAnswers) {
      if (answer == possibleAnswer) endSearch = true;
    }
  }
  return answer;
}

Future<List<FileSystemEntity>> dirContents(Directory dir) {
  final files = <FileSystemEntity>[];
  final completer = Completer<List<FileSystemEntity>>();
  final lister = dir.list(recursive: true);
  lister.listen((file) => files.add(file),
      // should also register onError
      onDone: () => completer.complete(files));
  return completer.future;
}

class Stach {
  final String content;
  final File file;
  final Match match;
  final String matchString;
  final String problem;

  Stach(this.content, this.file, this.match, this.matchString, this.problem);
}

extension KeyByValue on Map {
  keyByValue(value) {
    return this.keys.firstWhere((k) => this[k] == value, orElse: () => null);
  }
}

Map<String, dynamic> loadConfigFile(String path, String fileOptionResult) {
  final file = File(path);
  final yamlString = file.readAsStringSync();
  final Map yamlMap = loadYaml(yamlString);

  final config = <String, dynamic>{};
  for (MapEntry<dynamic, dynamic> entry in yamlMap['flutter_serve'].entries) {
    config[entry.key] = entry.value;
  }

  return config;
}

class TranslationsModel {
  Data data;

  TranslationsModel({this.data});

  TranslationsModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Translations> translations;

  Data({this.translations});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['translations'] != null) {
      translations = <Translations>[];
      json['translations'].forEach((v) {
        translations.add(Translations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (translations != null) {
      data['translations'] = translations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Translations {
  String translatedText;
  String detectedSourceLanguage;

  Translations({this.translatedText, this.detectedSourceLanguage});

  Translations.fromJson(Map<String, dynamic> json) {
    translatedText = json['translatedText'];
    detectedSourceLanguage = json['detectedSourceLanguage'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['translatedText'] = translatedText;
    data['detectedSourceLanguage'] = detectedSourceLanguage;
    return data;
  }
}

class GoogleTranslator {
  final String key;

  GoogleTranslator(this.key);

  translate(text, {from, to}) async {
    //https://translation.googleapis.com/language/translate/v2?target={YOUR_LANGUAGE}&key=${API_KEY}&q=${TEXT}
    var request = Uri.https('translation.googleapis.com',
        '/language/translate/v2', {'q': text, 'key': key, 'target': to});

    var response = await http.get(request);
    return TranslationsModel.fromJson(jsonDecode(response.body))
        .data
        .translations
        .first
        .translatedText;
  }
}
