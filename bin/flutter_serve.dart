import 'dart:io';

import 'package:args/args.dart';
import 'package:args/command_runner.dart';
import 'package:dart_style/dart_style.dart';

import 'controller.dart';
import 'modules/database_processor/database_processor.dart';
import 'modules/database_processor/src/functions_and_consts.dart';
import 'modules/file_processor.dart';
import 'modules/pub_worker.dart';
import 'modules/string_processor.dart';
import 'package:resource_portable/resource.dart' show Resource;

void main(List<String> arguments) => chooseWhatYouWant(arguments);

void chooseWhatYouWant([List<String> arguments]) {
  print('ARGUMENTS: ${arguments}');

  if (arguments.isEmpty) {
    // print("EMPTY args");
    create();

    save(LaunchFlags.files.onlyName(), LaunchFlags.files.stringify());
    save(LaunchFlags.strings.onlyName(), LaunchFlags.strings.stringify());
    save(LaunchFlags.db.onlyName(), LaunchFlags.db.stringify());
    save(LaunchFlags.uploadDb2Web.onlyName(), LaunchFlags.uploadDb2Web.stringify());
    save(LaunchFlags.testDbLocally.onlyName(), LaunchFlags.testDbLocally.stringify());
    return;
  }
  final parser = ArgParser();
  parser.addFlag(LaunchFlags.files.onlyName(), callback: (isExist) {
    if (isExist) {
      // print('start file');
      fileWorker();
    }
  });
  parser.addFlag(LaunchFlags.strings.onlyName(), callback: (isExist) {
    if (isExist) {
      // print('start string');
      stringWorker();
    }
  });
  parser.addFlag(LaunchFlags.pub.onlyName(), callback: (isExist) {
    if (isExist) {
      // print('start string');
      pubWorker();
    }
  });
  parser.addFlag(LaunchFlags.db.stringify(), callback: (isExist) {
    if (isExist) {
      print('start db');
      databaseGen();
    }
  });
  parser.addFlag(LaunchFlags.uploadDb2Web.stringify(), callback: (isExist) {
    if (isExist) {
      print('start uploadDb2Web');
    }
  });
  parser.addFlag(LaunchFlags.testDbLocally.stringify(), callback: (isExist) {
    if (isExist) {
      print('start testDbLocally');
    }
  });
  parser.parse(arguments);

  // print('Ты хочешь преобразовать файлы(files|0) или строчки(strings|1), или базу данных(db|2)'
  //     ' или всё(all)?');
  // var answer = stdin.readLineSync().trim();
  // if (answer.contains('files') || answer.contains('0') || answer.contains('all')) {
  //   fileWorker();
  // }
  // if (answer.contains('strings') || answer.contains('1') || answer.contains('all')) {
  //   stringWorker();
  // }
  // if (answer.contains('db') || answer.contains('2') || answer.contains('all')) {
  //   databaseGen();
  // }
}
